import os
import string
import types
import typing as t

from ..config import BIN, ETC, MAKE, PETSC_DIR, PETSC_PREFIX, STATIC
from ..type import DictStr, Strings
from ..util import config, logger, mkdir, run, split2

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = te.ParamSpec('P')


class Environ:
    def __init__(self, arch: str) -> None:
        self._arch = arch
        self._bashrc = mkdir(ETC) / f'bashrc.{arch}.sh'
        self._data = {}

    def __enter__(self) -> 'te.Self':
        self.loads()
        os.environ.update(self._data)
        return self

    def __exit__(
        self,
        exc_type: t.Optional[t.Type[BaseException]] = None,
        exc_val: t.Optional[BaseException] = None,
        exc_tb: t.Optional[types.TracebackType] = None,
    ) -> None:
        self.bashrc()

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def render(self, options: Strings) -> Strings:
        for func in [
            lambda option: option.replace('<ensp>', ' '),
            lambda option: string.Template(option).safe_substitute(os.environ),
        ]:
            options = map(func, options)
        return list(options)

    def bashrc(self) -> None:
        bins = ['$PETSC_USER_BIN', '$PETSC_PREFIX/bin', '$PETSC_DIR/$PETSC_ARCH/bin']
        prefixes = ['CC', 'PCC', 'PETSC']
        stdout: bytes = run.dc(f'{MAKE}', cwd=STATIC, capture_output=True).stdout
        self._data.update({
            key: value
            for key, value in map(split2, stdout.decode().splitlines())
            if any(map(key.startswith, prefixes))
        })
        # finalize
        lines = self._exports(self._data) + self._paths(*bins)
        self._bashrc.write_text('\n'.join(lines)+'\n')

    def loads(self) -> None:
        for func in [self._load_environ, self._load_default, self._load_pyproject]:
            self._data.update(func())
        # render
        for _ in range(9):
            flag = True
            for key, old in self._data.items():
                new = string.Template(old).safe_substitute(self._data)
                flag &= old == new
                self._data[key] = new
            if flag:
                break
        else:
            logger.error('RecursionError: maximum recursion depth exceeded')
            exit(1)

    def _load_environ(self) -> DictStr[str]:
        prefixes = ['CC', 'PCC', 'PETSC']
        return {
            key: value
            for key, value in os.environ.items()
            if any(map(key.startswith, prefixes))
        }

    def _load_default(self) -> DictStr[str]:
        return {
            'PETSC_ARCH': self._arch,
            'PETSC_DIR': PETSC_DIR.as_posix(),
            'PETSC_PREFIX': f'{PETSC_PREFIX.as_posix()}/$PETSC_ARCH',
            'PETSC_USER_BIN': mkdir(BIN/self._arch).as_posix(),
        }

    def _load_pyproject(self) -> DictStr[str]:
        return config.environ()

    def _export(self, key: str, value: str) -> str:
        if not (value.startswith('"') and value.endswith('"')):
            value = f'"{value}"'
        return f'export {key}={value}'

    def _exports(self, data: DictStr[str]) -> Strings:
        return [self._export(key, value) for key, value in data.items()]

    def _path(self, value: str) -> str:
        # https://superuser.com/questions/39751/add-directory-to-path-if-its-not-already-there
        return '\n'.join([
            f'if [ -d "{value}" ] && [[ ":$PATH:" != *":{value}:"* ]]; then',
            f'    PATH="{value}${{PATH:+":$PATH"}}"',
            f'fi',
        ])

    def _paths(self, *values: str) -> Strings:
        return list(map(self._path, values))
