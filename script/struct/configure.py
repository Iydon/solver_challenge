import importlib as i
import sys
import typing as t

from iydon.util.rust.std.result import Result

from .environ import Environ
from ..config import MAKE, PETSC_DIR, PETSC_PREFIX
from ..type import Pair, Strings
from ..util import config, logger, run

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = te.ParamSpec('P')


class Configure:
    '''https://petsc.org/release/install/install/'''

    def __init__(
        self,
        env: Environ, arch_src: str, arch_dst: str,
        check: bool, dry_run: bool, force: bool, options: Strings,
    ) -> None:
        self._options = env.render(self._arch(arch_src)+list(options))
        self._checked = check
        self._call = logger.info if dry_run else run.petsc
        self._flag = (PETSC_PREFIX/arch_dst).exists() and not (dry_run or force)

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def api(self) -> 'te.Self':
        if self._flag:
            logger.info('Use "--force" to configure and make anyway')
        else:
            self.configure()
            self.make()
        return self

    def configure(self) -> None:
        options = self._options
        if self._checked:
            options, invalids = self._check(options)
            if invalids:
                logger.warning(f'Invalid options: {invalids}')
        self._call('./configure ' + ' '.join(options))

    def make(self) -> None:
        self._call(f'{MAKE} all check install')

    def _arch(self, arch: str) -> Strings:
        return Result.err([]) \
            .or_else(lambda err: self._arch_pyproject(arch, err)) \
            .or_else(lambda err: self._arch_petsc(arch, err)) \
            .unwrap_or_else(self._arch_err)

    def _arch_err(self, err: Strings) -> None:
        list(map(logger.error, err))
        exit(1)

    def _arch_petsc(self, arch: str, err: Strings) -> Result[Strings, Strings]:
        sys.path.append(PETSC_DIR.as_posix())
        try:
            module = i.import_module(f'config.examples.{arch}')
        except ModuleNotFoundError:
            return Result.err([*err, f'[petsc] arch "{arch}" not found in example directory'])
        options = getattr(module, 'configure_options', None)
        if options is None or not isinstance(options, list):
            return Result.err([*err, f'[petsc] attribution "configure_options" of arch "{arch}" makes no sense'])
        else:
            return Result.ok(options)

    def _arch_pyproject(self, arch: str, err: Strings) -> Result[Strings, Strings]:
        options = config.petsc().get(arch, None)
        if options is None or not isinstance(options, list):
            return Result.err([*err, f'[pyproject] arch "{arch}" in configuration makes no sense'])
        else:
            return Result.ok(options)

    def _check(self, options: Strings) -> Pair[Strings, Strings]:
        cp = run.petsc('./configure --help', capture_output=True)
        help: str = cp.stdout.decode()
        valids, invalids = [], []
        for option in options:
            key = option.split('=')[0]
            if key in help:
                valids.append(option)
            else:
                invalids.append(option)
        return valids, invalids
