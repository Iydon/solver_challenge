import typing as t

from iydon.util.rust.std.option import NONE, Option

from ..type import DictInt, SomeInt, Strings, Triple

if t.TYPE_CHECKING:
    import typing_extensions as te


class Manager:
    def __init__(self, versions: t.Iterable['Version']) -> None:
        self._versions: DictInt[DictInt[DictInt[Version]]] = {}
        for version in versions:
            self._versions \
                .setdefault(version.major, {}) \
                .setdefault(version.minor, {}) \
                .setdefault(version.micro, version)

    @classmethod
    def fromStrings(cls, strings: Strings) -> 'te.Self':
        return cls(
            version.unwrap()
            for version in map(Version.fromString, strings)
            if version.is_some()
        )

    @classmethod
    def fromTags(cls, tags: Strings) -> 'te.Self':
        return cls(
            version.unwrap()
            for version in map(Version.fromTag, tags)
            if version.is_some()
        )

    @property
    def versions(self) -> t.List['Version']:
        # latest -> oldest
        return sorted(self._versions_iter(), reverse=True)

    def search_latest(
        self,
        major: SomeInt = None, minor: SomeInt = None, micro: SomeInt = None,
    ) -> Option['Version']:
        tmp = self._versions
        for part in map(Option.new, (major, minor, micro)):
            key = part.unwrap_or_else(lambda: max(tmp.keys()))
            tmp = tmp.get(key, None)
            if tmp is None:
                return NONE
        return Option.some(tmp)

    def search_latest_from_string(self, string: str = '') -> Option['Version']:
        try:
            args = map(int, string.split('.')) if string else ()
            return self.search_latest(*args)
        except (TypeError, ValueError):
            return NONE

    def search_latest_from_tag(self, tag: str = '') -> Option['Version']:
        return self.search_latest_from_string(tag.lstrip('v'))

    def _versions_iter(self) -> t.Iterator['Version']:
        for minors in self._versions.values():
            for micros in minors.values():
                yield from micros.values()


class Version(t.NamedTuple):
    major: int
    minor: int
    micro: int

    def __lt__(self, other: 'te.Self') -> bool:
        return self.to_tuple() < other.to_tuple()

    def __gt__(self, other: 'te.Self') -> bool:
        return other.__lt__(self)

    @classmethod
    def fromString(cls, string: str) -> Option['te.Self']:
        try:
            self = cls(*map(int, string.split('.')))
        except (TypeError, ValueError):
            return NONE
        else:
            return Option.some(self)

    @classmethod
    def fromTag(cls, tag: str) -> Option['te.Self']:
        return cls.fromString(tag.lstrip('v'))

    def to_tuple(self) -> Triple[int, int, int]:
        # or use tuple directly
        return (self.major, self.minor, self.micro)

    def to_string(self) -> str:
        return f'{self.major}.{self.minor}.{self.micro}'

    def to_tag(self) -> str:
        return f'v{self.to_string()}'
