import json
import os
import pathlib as p
import typing as t

from ...type import Path
from ...util import mkdir

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = t.ParamSpec('P')


class VSCode:
    '''
    References:
        - https://code.visualstudio.com/docs/cpp/c-cpp-properties-schema-reference
        - https://code.visualstudio.com/docs/cpp/launch-json-reference
    '''

    def __init__(self, name: str) -> None:
        self._name = name

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def save(self, directory: Path) -> None:
        directory = mkdir(p.Path(directory, '.vscode'))
        data = {
            'c_cpp_properties.json': self._c_cpp_properties,
            'launch.json': self._launch,
        }
        for filename, func in data.items():
            (directory/filename).write_text(func())

    def _c_cpp_properties(self) -> str:
        prefix = os.environ['PETSC_PREFIX']
        return json.dumps({
            'configurations': [
                {
                    'name': 'Linux',
                    'includePath': [
                        f'{prefix}/include',
                    ],
                    'defines': [],
                    'compilerPath': f'{prefix}/bin/mpicc',
                    'intelliSenseMode': 'linux-gcc-x64',
                    'cStandard': 'c17',
                    'cppStandard': 'c++14',
                },
            ],
            'version': 4,
        }, indent=4)

    def _launch(self) -> str:
        return json.dumps({
            'configurations': [
                {
                    'name': self._name,
                    'type': 'cppdbg',
                    'request': 'launch',
                    'program': '${workspaceFolder}/build/TODO',
                    'args': ['-log_view'],
                    'cwd': '${fileDirname}',
                    'environment': [
                        {
                            'name': 'PETSC_FOUND',
                            'value': '1',
                        },
                    ],
                    'MIMode': 'gdb',
                },
            ],
        }, indent=4)
