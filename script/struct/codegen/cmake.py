import os
import pathlib as p
import re
import shutil
import typing as t

from ...config import PETSC_DIR
from ...type import DictStr, Path, Strings
from ...util import config, logger

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = t.ParamSpec('P')
    Kwargs = te.ParamSpecKwargs(P)


class CMake:
    def __init__(self, name: str) -> None:
        self._name = name
        self._demos = self._load_demos()
        self._lines = []
        self._pattern = re.compile(r'( |^)(-l)([^ ]+)')

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def render(self) -> str:
        ans, old = '', ''
        for new in self._lines:
            if not os.path.commonprefix([old, new]):
                ans += '\n'
            ans += new + '\n'
            old = new
        return ans.lstrip()

    def code(self) -> str:
        self \
            .cmake_minimum_required() \
            .project(self._name) \
            .sets(
                CMAKE_C_COMPILER='mpicc', CMAKE_CXX_COMPILER='mpicxx',
                SOURCES=' '.join(self._demos.keys()),
            ) \
            .add_definitions('$ENV{PCC_FLAGS}') \
            .include_directories('$ENV{PETSC_PREFIX}/include') \
            .link_directories('$ENV{PETSC_PREFIX}/lib')
        self._lines.append('''
foreach(SOURCE IN LISTS SOURCES)
    get_filename_component(STEM ${SOURCE} NAME_WE)
    add_executable(${STEM} ${SOURCE})
    target_link_libraries(${STEM} $ENV{PETSC_LIB})
    install(TARGETS ${STEM} RUNTIME DESTINATION $ENV{PETSC_USER_BIN})
endforeach()
        '''.strip())
        return self.render()

    def save(self, directory: Path) -> None:
        directory = p.Path(directory)
        for filename, src in self._demos.items():
            dst = directory / filename
            if not dst.exists():
                shutil.copyfile(src, directory/dst)
        (directory/'CMakeLists.txt').write_text(self.code())

    def cmake_minimum_required(self, version: str = '3.12') -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/cmake_minimum_required.html
        return self._append('cmake_minimum_required', 'VERSION', version)

    def project(self, name: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/project.html
        return self._append('project', name)

    def set(self, key: str, value: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/set.html
        return self._append('set', key, value)

    def sets(self, **kwargs: 'Kwargs') -> 'te.Self':
        for key, value in kwargs.items():
            self.set(key, value)
        return self

    def add_definitions(self, *definitions: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/add_definitions.html
        return self._append('add_definitions', *definitions)

    def include_directories(self, *values: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/include_directories.html
        return self._append('include_directories', *values)

    def link_directories(self, *values: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/link_directories.html
        return self._append('link_directories', *values)

    def add_executable(self, name: str, *sources: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/add_executable.html
        return self._append('add_executable', name, *sources)

    def target_link_libraries(self, target: str, *items: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/target_link_libraries.html
        return self._append('target_link_libraries', target, *items)

    def install(self, target: str, directory: str) -> 'te.Self':
        # https://cmake.org/cmake/help/latest/command/install.html
        return self._append('install', 'TARGETS', target, 'RUNTIME', 'DESTINATION', directory)

    def _load_demos(self) -> DictStr[p.Path]:
        ans = {}
        data = config.code()
        excludes = data.get('excludes', [])
        join = lambda path: '-'.join(path.parts)
        for pattern in data.get('includes', []):
            for path in PETSC_DIR.glob(pattern):
                relative = path.relative_to(PETSC_DIR)
                if path.exists() and relative.as_posix() not in excludes:
                    ans[join(relative)] = path
        return ans

    def _append(self, command: str, *values: str) -> 'te.Self':
        self._lines.append(f'{command}({" ".join(values)})')
        return self

    def _definitions(self) -> Strings:
        environs = ['PCC_FLAGS', 'CC_FLAGS']
        for environ in environs:
            definitions = os.environ.get(environ, '')
            if definitions:
                break
        else:
            logger.warning(f'Environment variables not found: {", ".join(environs)}')
        return definitions.split()

    def _target_link_libraries(self) -> DictStr[str]:
        libraries = {}
        prefixes, suffixes = ['PETSC'], ['BASIC', 'LIB']
        for key, value in os.environ.items():
            if any(map(key.startswith, prefixes)) and any(map(key.endswith, suffixes)):
                libraries[f'ITEM_{key}'] = ' '.join(
                    library[-1]
                    for library in self._pattern.findall(value)
                )
        return libraries
