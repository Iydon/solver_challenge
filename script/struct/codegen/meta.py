import json
import os
import pathlib as p
import typing as t

from ...config import MAKE
from ...type import DictStr, Func0, Path, Strings
from ...util import logger, run

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = te.ParamSpec('P')


class Meta:
    def __init__(self, directory: Path) -> None:
        self._path = p.Path(directory, 'meta.json')
        if self._path.exists():
            self._data = json.loads(self._path.read_text())
        else:
            self._data = self._default()

    def __contains__(self, key: str) -> bool:
        return self._data.__contains__(key)

    def __getitem__(self, key: str) -> Func0[None]:
        key, *args = key.split('.')
        return lambda: getattr(self, f'_api_{key}')(*args, data=self._data[key])

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def apply(self, key: str) -> None:
        try:
            self[key]()
        except (AttributeError, KeyError):
            logger.error(f'Action "{key}" does not exist')

    def save(self) -> None:
        self._update()
        self._path.write_text(json.dumps(self._data, indent=4))

    def _default(self) -> DictStr:
        return {
            'info': {
                'arch': [],
            },
            'make': None,
        }

    def _update(self) -> None:
        # info
        arches = self._data['info']['arch']
        arch = os.environ.get('PETSC_ARCH', None)
        if not (arch is None or arch in arches):
            logger.warning(f'Add arch "{arch}" to {self._path}')
            self._data['info']['arch'] = sorted([arch, *arches])

    def _run(self, command: str) -> None:
        run.dc(command, cwd=self._path.parent)

    def _api_cmd(self, *subs: str, data: DictStr[Strings]) -> None:
        list(map(self._run, data['.'.join(subs)]))

    def _api_info(self, data: DictStr) -> None:
        for key, value in data.items():
            logger.info(f'{key}: {value}')

    def _api_make(self, *subs: str, data: None) -> None:
        self._run(f'{MAKE} {" ".join(subs)}')
