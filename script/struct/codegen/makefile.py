import pathlib as p
import typing as t

from ...config import CMAKE, MAKE
from ...type import Path, Strings

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = t.ParamSpec('P')
    Kwargs = te.ParamSpecKwargs(P)


class Makefile:
    def __init__(self, name: str) -> None:
        self._name = name
        self._lines = []

    @classmethod
    def new(cls, *args: 'P.args', **kwargs: 'P.kwargs') -> 'te.Self':
        return cls(*args, **kwargs)

    def render(self) -> str:
        return '\n\n'.join(self._lines) + '\n'

    def code(self) -> str:
        # https://petsc.org/release/manual/getting_started/
        commands = {
            'build': ['mkdir --parents ${BUILD}', 'cd ${BUILD} && ${CMAKE} .. && ${MAKE}'],
            'clean': ['rm --recursive --force ${BUILD}'],
            'install': ['cd ${BUILD} && ${MAKE} install'],
        }
        return self \
            .sets(BUILD='build', CMAKE=CMAKE, MAKE=MAKE) \
            .phony(*commands.keys()) \
            .rule('build', [], commands['build']) \
            .rule('clean', [], commands['clean']) \
            .rule('install', ['build'], commands['install']) \
            .render()

    def save(self, directory: Path) -> None:
        (p.Path(directory, 'Makefile')).write_text(self.code())

    def set(self, key: str, value: str) -> 'te.Self':
        return self._append(self._set(key, value))

    def sets(self, **kwargs: 'Kwargs') -> 'te.Self':
        line = '\n'.join(self._set(key, value) for key, value in kwargs.items())
        return self._append(line)

    def phony(self, *targets: str) -> 'te.Self':
        return self._append(f'.PHONY: {" ".join(targets)}')

    def rule(self, target: str, dependencies: Strings, commands: Strings) -> 'te.Self':
        body = '\n'.join(map('\t{}'.format, commands))
        header = f'{target}: {" ".join(dependencies)}'.strip()
        return self._append(f'{header}\n{body}')

    def _append(self, line: str) -> 'te.Self':
        self._lines.append(line.strip())
        return self

    def _set(self, key: str, value: str) -> str:
        return f'{key} = {value}'
