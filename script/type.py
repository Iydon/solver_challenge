import pathlib as p
import typing as t


T1, T2, T3 = t.TypeVar('T1'), t.TypeVar('T2'), t.TypeVar('T3')

Command = t.Union[str, t.List[str]]
Integers = t.List[int]
Path = t.Union[str, p.Path]
SomeInt = t.Optional[int]
SomeStr = t.Optional[str]
Strings = t.List[str]

DictInt = t.Dict[int, T1]
DictStr = t.Dict[str, T1]
Func0 = t.Callable[[], T1]
Func1 = t.Callable[[T1], T2]
Func2 = t.Callable[[T1, T2], T3]
Pair = t.Tuple[T1, T2]
Triple = t.Tuple[T1, T2, T3]
TupleSeq = t.Tuple[T1, ...]
