import importlib as _i
import typing as _t

import click as _c

if _t.TYPE_CHECKING:
    import typing_extensions as te

    P = te.ParamSpec('P')


@_c.command()
def init(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Initialize project (placeholder only)'''
    _i.import_module('.command.init', __package__).api(*args, **kwargs)


@_c.command()
@_c.option('-n', '--name', type=str, default='demo', help='directory src/<name>/')
@_c.option('-c', '--create', is_flag=True)
@_c.option('-uc', '--update-cmake', is_flag=True)
@_c.option('-um', '--update-makefile', is_flag=True)
@_c.option('-uv', '--update-vscode', is_flag=True)
def code(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Initialize source code

    \b
    $ ... code --name demo --create

    \b
    $ ... code --name bin --update-vscode
    '''
    _i.import_module('.command.code', __package__).api(*args, **kwargs)


@_c.command()
@_c.option('-s', '--style', type=str, default='Google')
@_c.option('-i', '--indent', type=int, default=4)
@_c.option('-c', '--column', type=int, default=128)
def format(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Auto format C source code'''
    _i.import_module('.command.format', __package__).api(*args, **kwargs)


@_c.command()
@_c.option('-n', '--name', type=str, default='demo', help='directory src/<name>/')
@_c.option('-a', '--action', type=str, default='info')
def meta(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Actions from meta.json

    \b
    $ ... meta --name demo --action info

    \b
    $ ... meta --name bin --action cmd.install

    \b
    $ ... meta --name prob1 --action make.test
    '''
    _i.import_module('.command.meta', __package__).api(*args, **kwargs)

@_c.command()
@_c.option('-v', '--version', type=str, default='', help='$(major) or $(major).$(minor) or $(major).$(minor).$(micro)')
@_c.option('-a', '--arches', multiple=True, type=str, default=['default'], help='petsc/config/examples/<arch>.py')
@_c.option('-u', '--check', is_flag=True)
@_c.option('-d', '--dry-run', is_flag=True)
@_c.option('-f', '--force', is_flag=True)
@_c.argument('options', nargs=-1, type=str)
def petsc(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Build script for PETSc

    \b
    $ ... petsc  # latest version of vx.x.x (v3.19.2)
    HEAD is now at e03e79094e0 Increase patchlevel to 3.19.2

    \b
    $ ... petsc --version 2  # latest version of v2.x.x (2.3.3)
    HEAD is now at 8ae49da34c5 Release 2.3.3

    \b
    $ ... petsc --version 3.14  # latest version of v3.14.x (v3.14.6)
    HEAD is now at 096591eb87f Increase patchlevel to 3.14.6

    \b
    $ ... petsc --version 3.18.6  # specified version of v3.18.6
    HEAD is now at c7d19d7b3f2 Increase patchlevel to 3.18.6

    \b
    $ ... petsc --version 0.0.0  # incorrect version
    Version "0.0.0" is incorrect, available versions:
    3.19.2  3.19.1  3.19.0  3.18.6  3.18.5  3.18.4  3.18.3  3.18.2  3.18.1  3.18.0  3.17.5  3.17.4  3.17.3  3.17.2  3.17.1  3.17.0  3.16.6  3.16.5  3.16.4  3.16.3  3.16.2  3.16.1  3.16.0  3.15.5  3.15.4  3.15.3  3.15.2  3.15.1  3.15.0  3.14.6  3.14.5  3.14.4  3.14.3  3.14.2  3.14.1  3.13.6  3.13.5  3.13.4  3.13.3  3.13.2  3.13.1  3.12.5  3.12.4  3.12.3  3.12.2  3.12.1  3.11.4  3.11.3  3.11.2  3.11.1  3.10.5  3.10.4  3.10.3  3.10.2  3.10.1  3.9.4  3.9.3  3.9.2  3.9.1  3.8.4  3.8.3  3.8.2  3.8.1  3.7.7  3.7.6  3.7.5  3.7.4  3.7.3  3.7.2  3.7.1  3.6.4  3.6.3  3.6.2  3.6.1  3.5.4  3.5.3  3.5.2  3.5.1  3.4.5  3.4.4  3.4.3  3.4.2  3.4.1  3.0.0  2.3.3  2.3.2  2.1.0  2.0.29

    \b
    $ ... petsc --arches advance-real-optimize --arches advance-complex-optimize --force -- --with-precision=double
    '''
    _i.import_module('.command.petsc', __package__).api(*args, **kwargs)


@_c.command()
def uncache(*args: 'P.args', **kwargs: 'P.kwargs') -> None:
    '''Remove project and python caches'''
    _i.import_module('.command.uncache', __package__).api(*args, **kwargs)
