import shutil

from ..config import BIN, CACHE, ETC, ROOT


def api() -> None:
    rmtree = lambda path: shutil.rmtree(path, ignore_errors=True)
    # project
    list(map(rmtree, [BIN, CACHE, ETC]))
    # python
    for path in ROOT.rglob('__pycache__'):
        rmtree(path)
