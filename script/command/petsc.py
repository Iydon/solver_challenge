from ..config import GIT, PETSC_DIR
from ..type import Strings
from ..util import logger, mkdir, run, split2
from ..struct.configure import Configure
from ..struct.environ import Environ
from ..struct.version import Manager, Version


def api(version: str, arches: Strings, check: bool, dry_run: bool, force: bool, options: Strings) -> None:
    # initialize
    if not PETSC_DIR.exists():
        mkdir(PETSC_DIR.parent)
        run.dc(f'{GIT} clone --branch release https://gitlab.com/petsc/petsc {PETSC_DIR}')
        run.petsc(f'{GIT} pull')
    # search version
    stdout: bytes = run.petsc(f'{GIT} tag', capture_output=True).stdout
    manager = Manager.fromTags(stdout.decode().splitlines())
    target = manager.search_latest_from_tag(version)
    if target.is_none():
        versions = '  '.join(map(Version.to_string, manager.versions))
        logger.error(f'Version "{version}" is incorrect, available versions:\n{versions}')
        exit(1)
    run.petsc(f'{GIT} checkout {target.unwrap().to_tag()}')
    # export and configure
    for arch in arches:
        src, dst = split2(arch, ':', arch)
        with Environ.new(dst) as env:
            Configure.new(env, src, dst, check, dry_run, force, options).api()
