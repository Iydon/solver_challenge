from ..config import SRC
from ..util import logger
from ..struct.codegen.meta import Meta


def api(name: str, action: str) -> None:
    src = SRC / name
    if not src.exists():
        logger.error(f'Directory "{src}" does not exist')
        return
    Meta.new(src).apply(action)
