import tqdm

from ..config import CLANG_FORMAT, SRC
from ..util import logger, run


def api(style: str, indent: int, column: int) -> None:
    paths = [
        path
        for path in SRC.rglob('*')
        if path.suffix in {'.c', '.h'} \
            and 'build' not in path.relative_to(SRC).parts
    ]
    for path in tqdm.tqdm(paths):
        config = f'"{{BasedOnStyle: {style}, IndentWidth: {indent}, ReflowComments: false, ColumnLimit: {column}}}"'
        logger.info(path)
        run.d(f'{CLANG_FORMAT} --style={config} {path} | diff {path} -')
