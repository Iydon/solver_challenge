from ..config import SRC
from ..util import mkdir
from ..struct.codegen.cmake import CMake
from ..struct.codegen.makefile import Makefile
from ..struct.codegen.meta import Meta
from ..struct.codegen.vscode import VSCode


def api(
    name: str, create: bool,
    update_cmake: bool, update_makefile: bool, update_vscode: bool,
) -> None:
    src = mkdir(SRC / name)
    # meta
    Meta.new(src).save()
    # create, update
    if create:
        update_cmake = update_makefile = update_vscode = True
    if update_cmake:
        CMake.new(name).save(src)
    if update_makefile:
        Makefile.new(name).save(src)
    if update_vscode:
        VSCode.new(name).save(src)
