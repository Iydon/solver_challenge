import pathlib as p


HOME = p.Path.home()
ROOT = p.Path(__file__).absolute().parents[1]

BIN = ROOT / 'bin'
CACHE = HOME / '.cache' / 'solver_challenge'
ETC = ROOT / 'etc'
PYPROJECT = ROOT / 'pyproject.toml'
SCRIPT = ROOT / 'script'
SRC = ROOT / 'src'
STATIC = SCRIPT / 'static'
TARGET = CACHE / 'target'

PETSC_DIR = CACHE / 'git'
PETSC_PREFIX = CACHE / 'target'

CLANG_FORMAT = 'clang-format'
CMAKE = 'cmake'
GIT = 'git'
MAKE = 'make'
