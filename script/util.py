import pathlib as p
import subprocess as sp
import typing as t

from iydon.util.rust.std.option import NONE, Option
from loguru import logger
from tomlkit import TOMLDocument, parse

from .config import ROOT, PETSC_DIR, PYPROJECT
from .type import Command, DictStr, Pair, Path, Strings

if t.TYPE_CHECKING:
    import typing_extensions as te

    P = te.ParamSpec('P')
    Kwargs = te.ParamSpecKwargs(P)


class config:
    _pyproject = None

    @classmethod
    def pyproject(cls) -> TOMLDocument:
        if cls._pyproject is None:
            cls._pyproject = parse(PYPROJECT.read_text())
        return cls._pyproject

    @classmethod
    def environ(cls) -> DictStr[str]:
        return cls.pyproject()['tool']['script']['environ']

    @classmethod
    def code(cls) -> DictStr[str]:
        return cls.pyproject()['tool']['script']['config']['code']

    @classmethod
    def petsc(cls) -> DictStr[Strings]:
        ans: DictStr[DictStr[str]] = {}
        petsc: DictStr[Pair[Strings, Strings]] = \
            cls.pyproject()['tool']['script']['config']['petsc']
        for arch, (bases, options) in petsc.items():
            ans[arch] = {}
            for base in bases:
                ans[arch].update(ans.get(base, {}))
            ans[arch].update({
                key: value
                for key, value in map(lambda option: split2(option, '=', '1'), options)
            })
        return {
            key: list(map('='.join, values.items()))
            for key, values in ans.items()
        }


class run:
    @classmethod
    def origin(cls, command: Command, **kwargs: 'Kwargs') -> sp.CompletedProcess:
        return sp.run(command, **kwargs)

    @classmethod
    def origin_check(cls, command: Command, **kwargs: 'Kwargs') -> sp.CompletedProcess:
        cp = cls.origin(command, **kwargs)
        assert cp.returncode == 0, cp.stderr
        return cp

    @classmethod
    def default(cls, command: Command, **kwargs: 'Kwargs') -> sp.CompletedProcess:
        kwargs = {'shell': True, 'cwd': ROOT, **kwargs}
        return cls.origin(command, **kwargs)

    @classmethod
    def default_check(cls, command: Command, **kwargs: 'Kwargs') -> sp.CompletedProcess:
        cp = cls.default(command, **kwargs)
        assert cp.returncode == 0, cp.stderr
        return cp

    @classmethod
    def petsc(cls, command: Command, **kwargs: 'Kwargs') -> sp.CompletedProcess:
        return cls.origin_check(command, shell=True, cwd=PETSC_DIR, **kwargs)

    o = origin
    oc = origin_check
    d = default
    dc = default_check


def mkdir(path: Path) -> p.Path:
    directory = p.Path(path)
    directory.mkdir(parents=True, exist_ok=True)
    return directory


def optionize(kwargs: DictStr, keys: Strings) -> None:
    for key in keys:
        value = kwargs.get(key, None)
        kwargs[key] = NONE if value is None else Option.some(value)


def split2(text: str, sep: str='=', default: str = '') -> Pair[str, str]:
    if text.count(sep):
        return text.split(sep, maxsplit=1)
    else:
        return (text, default)
