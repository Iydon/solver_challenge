#include <mpi.h>
#include <petscerror.h>
#include <petscksp.h>
#include <petscmat.h>
#include <petscsys.h>
#include <petscsystypes.h>
#include <petsctime.h>
#include <petscvec.h>
#include <petscviewer.h>
#include <petscviewertypes.h>

#include "util.h"

static const PetscChar help[] =
    "Test for KSPSolve (Ax=b)\n"
    "\n"
    "Options:\n"
    "  --A       TEXT  A matrix path\n"
    "  --b       TEXT  b vector path\n"
    "  --x       TEXT  solution x vector path\n"
    "  --rtol    REAL  relative convergence tolerance\n"
    "  --stdout        viewer stdout\n";

typedef struct {
    PetscBool is_stdout;
    PetscChar a[PETSC_MAX_PATH_LEN], b[PETSC_MAX_PATH_LEN], x[PETSC_MAX_PATH_LEN];
    PetscReal rtol;
    // flags
    PetscBool flag_a, flag_b, flag_x;
} AppCtx;

PetscErrorCode user_init_options(AppCtx *);
PetscErrorCode user_solve(AppCtx *);

int main(int argc, char **args) {
    AppCtx user;
    PetscCall(
           PetscInitialize(&argc, &args, (char *)0, help)
        || user_init_options(&user)
        || user_solve(&user)
        || PetscFinalize()
    );
    return 0;
}

PetscErrorCode user_init_options(AppCtx *ctx) {
    PetscCall(
           user_options_get_boolean_default("--stdout", &ctx->is_stdout)
        || user_options_get_path("--A", ctx->a, &ctx->flag_a)
        || user_options_get_path("--b", ctx->b, &ctx->flag_b)
        || user_options_get_path("--x", ctx->x, &ctx->flag_x)
        || user_options_get_real_default("--rtol", &ctx->rtol, 1.0e-7)
    );
    // a, b
    if (!(ctx->flag_a && ctx->flag_b)) {
        PetscPrintf(PETSC_COMM_WORLD, help);
        exit(0);
    }
    // stdout
    if (!ctx->flag_x) {
        ctx->is_stdout = PETSC_TRUE;
    }
    return PETSC_SUCCESS;
}

PetscErrorCode user_solve(AppCtx *ctx) {
    KSP ksp;
    Mat a;
    PC pc;
    PetscInt number;
    PetscLogDouble tic, toc;
    PetscReal norm;
    PetscViewer viewer;
    Vec b, x;
    PetscErrorCode ierr = PETSC_SUCCESS;
    // load
    ierr = ierr
        || PetscViewerBinaryOpen(PETSC_COMM_WORLD, ctx->a, FILE_MODE_READ, &viewer)
        || MatCreate(PETSC_COMM_WORLD, &a)
        || MatLoad(a, viewer)
        || PetscViewerDestroy(&viewer)
        || PetscViewerBinaryOpen(PETSC_COMM_WORLD, ctx->b, FILE_MODE_READ, &viewer)
        || VecCreate(PETSC_COMM_WORLD, &b)
        || VecLoad(b, viewer)
        || PetscViewerDestroy(&viewer);
    // solve
    ierr = ierr
        || VecDuplicate(b, &x)
        || KSPCreate(PETSC_COMM_WORLD, &ksp)
        || KSPSetFromOptions(ksp)
        || KSPGetPC(ksp, &pc)
        || PCSetFromOptions(pc)
        || KSPSetOperators(ksp, a, a)
        || KSPSetTolerances(ksp, ctx->rtol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT)
        || PetscTime(&toc)
        || KSPSolve(ksp, b, x)
        || PetscTime(&tic)
        || KSPGetResidualNorm(ksp, &norm)
        || KSPGetIterationNumber(ksp, &number)
        || PetscPrintf(PETSC_COMM_WORLD, "Residual %le with %d iterations (%le seconds)\n", norm, number, tic-toc);
    // post
    if (ctx->flag_x) {
        ierr = ierr
            || PetscViewerBinaryOpen(PETSC_COMM_WORLD, ctx->x, FILE_MODE_WRITE, &viewer)
            || VecView(x, viewer)
            || PetscViewerDestroy(&viewer);
    }
    if (ctx->is_stdout) {
        ierr = ierr || VecView(x, PETSC_VIEWER_STDOUT_WORLD);
    }
    return ierr
        || VecDestroy(&x)
        || VecDestroy(&b)
        || MatDestroy(&a)
        || KSPDestroy(&ksp);
}
