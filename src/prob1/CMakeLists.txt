cmake_minimum_required(VERSION 3.12)

project(prob1)

set(CMAKE_C_COMPILER mpicc)
set(CMAKE_CXX_COMPILER mpicxx)
add_definitions($ENV{PCC_FLAGS})
include_directories($ENV{PETSC_PREFIX}/include)
link_directories($ENV{PETSC_PREFIX}/lib)

set(PROGRAM_PROB1 ${PROJECT_NAME})
add_executable(${PROGRAM_PROB1} main.c)
target_link_libraries(${PROGRAM_PROB1} ${ITEM_PETSC_LIB})
install(TARGETS ${PROGRAM_PROB1} RUNTIME DESTINATION $ENV{PETSC_USER_BIN})
