// static const char *__all__[] = {
//     "log_log", "log_set_level",
//     "log_trace", "log_debug", "log_info", "log_warn", "log_error", "log_fatal",
//     "LOG_TRACE", "LOG_DEBUG", "LOG_INFO", "LOG_WARN", "LOG_ERROR", "LOG_FATAL",
//     "LOG_COLOR_BLUE", "LOG_COLOR_CYAN", "LOG_COLOR_GREEN", "LOG_COLOR_YELLOW",
//     "LOG_COLOR_RED", "LOG_COLOR_MAGENTA", "LOG_COLOR_GRAY", "LOG_COLOR_END",
// };

#pragma once

#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#define log_log(stream, level, ...) _log_log(stream, level, __FILE__, __LINE__, __VA_ARGS__)
#define log_trace(...) log_log(stderr, LOG_TRACE, __VA_ARGS__)
#define log_debug(...) log_log(stderr, LOG_DEBUG, __VA_ARGS__)
#define log_info(...) log_log(stderr, LOG_INFO, __VA_ARGS__)
#define log_warn(...) log_log(stderr, LOG_WARN, __VA_ARGS__)
#define log_error(...) log_log(stderr, LOG_ERROR, __VA_ARGS__)
#define log_fatal(...) log_log(stderr, LOG_FATAL, __VA_ARGS__)

enum {
    LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_FATAL,
};

static const char LOG_COLOR_BLUE[] = "\x1b[94m", LOG_COLOR_CYAN[] = "\x1b[36m",
    LOG_COLOR_GREEN[] = "\x1b[32m", LOG_COLOR_YELLOW[] = "\x1b[33m", LOG_COLOR_RED[] = "\x1b[31m",
    LOG_COLOR_MAGENTA[] = "\x1b[35m", LOG_COLOR_GRAY[] = "\x1b[90m", LOG_COLOR_END[] = "\x1b[0m";

static char _log_buffer[20];
static int _log_level_threshold = LOG_TRACE;
static const char *_log_strings[] = {
    "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL",
};
static const char *_log_colors[] = {
    LOG_COLOR_BLUE, LOG_COLOR_CYAN, LOG_COLOR_GREEN, LOG_COLOR_YELLOW, LOG_COLOR_RED, LOG_COLOR_MAGENTA,
};

void log_set_level(int level) {
    _log_level_threshold = level;
}

void _log_log(FILE *stream, int level, const char *file, int line, const char *fmt, ...) {
    if (level < _log_level_threshold) {
        return;
    }
    // time
    time_t t = time(NULL);
    size_t idx = strftime(_log_buffer, sizeof(_log_buffer), "%Y/%m/%d %H:%M:%S", localtime(&t));
    _log_buffer[idx] = '\0';
    // format
    fprintf(
        stream, "%s %s%5s%s %s%s:%d%s ",
        _log_buffer, _log_colors[level], _log_strings[level], LOG_COLOR_END, LOG_COLOR_GRAY, file, line, LOG_COLOR_END
    );
    va_list args;
    va_start(args, fmt);
    vfprintf(stream, fmt, args);
    va_end(args);
    fprintf(stream, "\n");
    fflush(stream);
}
